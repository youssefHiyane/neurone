package neurone.competence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import neurone.competence.entities.Entreprise;

@RepositoryRestResource @CrossOrigin("*") 
public interface EntrepriseRepository extends JpaRepository<Entreprise, Long>{

}
