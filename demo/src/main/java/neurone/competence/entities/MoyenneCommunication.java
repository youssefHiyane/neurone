package neurone.competence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class MoyenneCommunication implements Serializable{
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long idMoyenneComm;
	public Type type;
	private String info;
	private String sType;
	@ManyToOne
	private Employee employee;
	
	 enum Type {
		  Whatsapp,
		  Téléphone_Fix,
		  Portable,
		  Mail
		}
	 
	


	 
	public MoyenneCommunication(Long idMoyenneComm, Type type, String info, Employee employee) {
		this.idMoyenneComm = idMoyenneComm;
		this.type = type;
		this.info = info;
		this.employee = employee;
	}
	
	public MoyenneCommunication(Long idMoyenneComm, String stype, String info, Employee employee) {
		this.idMoyenneComm = idMoyenneComm;
		this.sType = stype;
		this.info = info;
		this.employee = employee;
	}


	public MoyenneCommunication() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getIdMoyenneComm() {
		return idMoyenneComm;
	}


	public void setIdMoyenneComm(Long idMoyenneComm) {
		this.idMoyenneComm = idMoyenneComm;
	}



	public String getInfo() {
		return info;
	}


	public void setInfo(String info) {
		this.info = info;
	}


	public Type getType() {
		return type;
	}


	public void setType(Type type) {
		this.type = type;
	}
	
	


	public String getsType() {
		return sType;
	}

	public void setsType(String sType) {
		this.sType = sType;
	}

	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	
	
	

}
