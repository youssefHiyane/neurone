package neurone.competence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Employee")
public class Employee implements Serializable{
	@Id @GeneratedValue(strategy = GenerationType.AUTO)	
	private Long idEmployee;
	private String firstName;
	private String lastName;
	private String email;
	private String adresse;
	@ManyToOne
	private Entreprise entreprise;
	
	
	

	
	public Employee() {

	}
	public Employee(Long idEmployee, String firstName, String lastName, String email, String adresse) {
	
		this.idEmployee = idEmployee;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.adresse = adresse;
	}
	
	
	public Employee(Long idEmployee, String firstName, String lastName, String email, String adresse,
			Entreprise entreprise) {
		super();
		this.idEmployee = idEmployee;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.adresse = adresse;
		this.entreprise = entreprise;
	}
	public Long getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(Long idEmployee) {
		this.idEmployee = idEmployee;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	

}
