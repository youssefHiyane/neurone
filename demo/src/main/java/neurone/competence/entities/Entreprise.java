package neurone.competence.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Entreprise implements Serializable{

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long idEntreprise;
	private String Libelle;
	private String LibelleCourt;
	private String FormeJuridique;
	private Integer nombreEmployee;
	
	
	
	
	public Entreprise() {
	}
	public Entreprise(Long idEntreprise, String libelle, String libelleCourt, String formeJuridique,
			Integer nombreEmployee) {
		this.idEntreprise = idEntreprise;
		Libelle = libelle;
		LibelleCourt = libelleCourt;
		FormeJuridique = formeJuridique;
		this.nombreEmployee = nombreEmployee;
	}
	public Long getIdEntreprise() {
		return idEntreprise;
	}
	public void setIdEntreprise(Long idEntreprise) {
		this.idEntreprise = idEntreprise;
	}
	public String getLibelle() {
		return Libelle;
	}
	public void setLibelle(String libelle) {
		Libelle = libelle;
	}
	public String getLibelleCourt() {
		return LibelleCourt;
	}
	public void setLibelleCourt(String libelleCourt) {
		LibelleCourt = libelleCourt;
	}
	public String getFormeJuridique() {
		return FormeJuridique;
	}
	public void setFormeJuridique(String formeJuridique) {
		FormeJuridique = formeJuridique;
	}
	public Integer getNombreEmployee() {
		return nombreEmployee;
	}
	public void setNombreEmployee(Integer nombreEmployee) {
		this.nombreEmployee = nombreEmployee;
	}

	
}
