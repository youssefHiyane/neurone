package neurone.competence.entities;

import java.io.Serializable;

import java.util.Date;




import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity @Table(name = "Utilsateur")
public class User implements Serializable {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	private String userId;
	private String firstName;
	private String lastName;
	private String userName;
	private String password;
	private String mail;
	private String profileImageUrl;
	private Date lasteLoginDate;
	private Date dateLoginDisplay;
	private Date joinDate;
	private String[] roles;
	private String[] authorities;
	private boolean isActive;
	private boolean isLocked;
	@OneToOne
	private Employee employee;
	
	
	public User() {

	}
	
	
	
	
	public User(Long id, String userId, String firstName, String lastName, String userName, String password,
			String mail, String profileImageUrl, Date lasteLoginDate, Date dateLoginDisplay, Date joinDate,
			String[] roles, String[] authorities, boolean isActive, boolean isLocked, Employee employee) {
		super();
		this.id = id;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.password = password;
		this.mail = mail;
		this.profileImageUrl = profileImageUrl;
		this.lasteLoginDate = lasteLoginDate;
		this.dateLoginDisplay = dateLoginDisplay;
		this.joinDate = joinDate;
		this.roles = roles;
		this.authorities = authorities;
		this.isActive = isActive;
		this.isLocked = isLocked;
		this.employee = employee;
	}




	public User(Long id, String userId, String firstName, String lastName, String userName, String password,
			String mail, String profileImageUrl, Date lasteLoginDate, Date dateLoginDisplay, Date joinDate,
			String[] roles, String[] authorities, boolean isActive, boolean isLocked) {
		super();
		this.id = id;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.password = password;
		this.mail = mail;
		this.profileImageUrl = profileImageUrl;
		this.lasteLoginDate = lasteLoginDate;
		this.dateLoginDisplay = dateLoginDisplay;
		this.joinDate = joinDate;
		this.roles = roles;
		this.authorities = authorities;
		this.isActive = isActive;
		this.isLocked = isLocked;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getProfileImageUrl() {
		return profileImageUrl;
	}
	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}
	public Date getLasteLoginDate() {
		return lasteLoginDate;
	}
	public void setLasteLoginDate(Date lasteLoginDate) {
		this.lasteLoginDate = lasteLoginDate;
	}
	public Date getDateLoginDisplay() {
		return dateLoginDisplay;
	}
	public void setDateLoginDisplay(Date dateLoginDisplay) {
		this.dateLoginDisplay = dateLoginDisplay;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public String[] getRoles() {
		return roles;
	}
	public void setRoles(String[] roles) {
		this.roles = roles;
	}
	public String[] getAuthorities() {
		return authorities;
	}
	public void setAuthorities(String[] authorities) {
		this.authorities = authorities;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public boolean isLocked() {
		return isLocked;
	}
	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}


	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	
}
