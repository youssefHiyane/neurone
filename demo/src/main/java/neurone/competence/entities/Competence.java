package neurone.competence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Competence implements Serializable{

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long idCompetence;
	private String libelle;
	private String liblleCourt;
	
	
	
	public Competence(Long idCompetence, String libelle, String liblleCourt) {
		this.idCompetence = idCompetence;
		this.libelle = libelle;
		this.liblleCourt = liblleCourt;
	}
	public Competence() {

	}
	public Long getIdCompetence() {
		return idCompetence;
	}
	public void setIdCompetence(Long idCompetence) {
		this.idCompetence = idCompetence;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getLiblleCourt() {
		return liblleCourt;
	}
	public void setLiblleCourt(String liblleCourt) {
		this.liblleCourt = liblleCourt;
	}
	
	
	
}
