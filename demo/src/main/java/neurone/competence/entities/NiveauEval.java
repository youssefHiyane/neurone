package neurone.competence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class NiveauEval implements Serializable{
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long idNiveauEval;
	@ManyToOne
	private Competence competence;
	@ManyToOne
	private Employee employee;
	public  String niveau;

	
	
	 public enum Niveau {
		  débutant,
		  moyen,
		  professionnel
		}
	 
	 
	 
	public NiveauEval() {
	}
	public NiveauEval(Long idNiveauEval, Competence idCompetence, Employee idEmplyee, String niveau) {
		this.idNiveauEval = idNiveauEval;
		this.competence = idCompetence;
		this.employee = idEmplyee;
		this.niveau = niveau;
	}

	public Long getIdNiveauEval() {
		return idNiveauEval;
	}
	public void setIdNiveauEval(Long idNiveauEval) {
		this.idNiveauEval = idNiveauEval;
	}

	public String getNiveau() {
		return niveau;
	}
	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	public Competence getCompetence() {
		return competence;
	}
	public void setCompetence(Competence competence) {
		this.competence = competence;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	

}
