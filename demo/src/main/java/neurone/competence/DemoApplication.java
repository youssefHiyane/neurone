package neurone.competence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import neurone.competence.dao.CompetenceRepositoty;
import neurone.competence.dao.EmployeeRepository;
import neurone.competence.dao.EntrepriseRepository;
import neurone.competence.dao.MoyenneCommunicationRepository;
import neurone.competence.dao.NiveauEvaluationRepository;
import neurone.competence.dao.UserRepository;
import neurone.competence.entities.Competence;
import neurone.competence.entities.Employee;
import neurone.competence.entities.Entreprise;
import neurone.competence.entities.MoyenneCommunication;
import neurone.competence.entities.NiveauEval;
import neurone.competence.entities.User;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	@Autowired
	private EmployeeRepository employeeRepo;
	@Autowired
	private EntrepriseRepository entrepriseRepo;
	@Autowired
	private CompetenceRepositoty competenceRepo;
	@Autowired
	private MoyenneCommunicationRepository communicationRepo;
	@Autowired
	private NiveauEvaluationRepository niveauEvalRepo;
	@Autowired
	private UserRepository userRepo;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Entreprise entreprise1 = new Entreprise(null, "Entreprise A", "Ent A", "SA", 250);
		Entreprise entreprise2 = new Entreprise(null, "Entreprise B", "Ent B", "SARL", 54);
		Entreprise entreprise3 = new Entreprise(null, "Entreprise C", "Ent C", "SA", 432);
		Entreprise entreprise4 = new Entreprise(null, "Entreprise D", "Ent D", "SARL", 23);
		entrepriseRepo.save(entreprise1);
		entrepriseRepo.save(entreprise2);
		entrepriseRepo.save(entreprise3);
		entrepriseRepo.save(entreprise4);
		
		

		Employee employee1 = new Employee(null, "Mohammed", "Zakaria", "amine.zak@gmail.com", "78 R 67 Rabat",
				entreprise1);
		Employee employee2 = new Employee(null, "Abdou", "Younes", "karim.yns@gmail.com", "902 R 67 Fes", entreprise3);
		employeeRepo.save(employee1);
		employeeRepo.save(employee2);
		employeeRepo.findAll().forEach(p -> {
			System.out.println(p.toString());

			User user1 = new User(null, "US1ADM", "Youssef", "Hiyane", "yhiyane", "TEST", "youssef.h@gmail.com", null, null, null, null, args, args, true, true, employee2);
			userRepo.save(user1);

			Competence comp1 = new Competence(null, "JAVA JEE ", "JEE");
			Competence comp2 = new Competence(null, "JANGULAR ", "ANGULAR");
			Competence comp3 = new Competence(null, "SPRING BOOT", "SB");
			Competence comp4 = new Competence(null, "REACT", "REACT");
			Competence comp5 = new Competence(null, "PYTHON", "PYTHON");
			Competence comp6 = new Competence(null, "RUBY ", "RUBY");
			Competence comp7 = new Competence(null, "NODE JS", "NODE JS");
			Competence comp8 = new Competence(null, "JSP SERVELET", "JSP/Servelet");

			competenceRepo.save(comp1);
			competenceRepo.save(comp2);
			competenceRepo.save(comp3);
			competenceRepo.save(comp4);
			competenceRepo.save(comp5);
			competenceRepo.save(comp6);
			competenceRepo.save(comp7);
			competenceRepo.save(comp8);

			communicationRepo.save(new MoyenneCommunication(null, "whatsapp", "0605983674", employee1));
			communicationRepo.save(new MoyenneCommunication(null, "Téléphone", "0327663733", employee1));
			communicationRepo.save(new MoyenneCommunication(null, "Téléphone", "0544377497", employee2));
			communicationRepo.save(new MoyenneCommunication(null, "Email", "abdou@abdou.com", employee2));

			niveauEvalRepo.save(new NiveauEval(null, comp1, employee1, "Moyenne"));
			niveauEvalRepo.save(new NiveauEval(null, comp3, employee1, "Pro"));
			niveauEvalRepo.save(new NiveauEval(null, comp5, employee2, "Débutant"));
			niveauEvalRepo.save(new NiveauEval(null, comp7, employee2, "Pro"));
			niveauEvalRepo.save(new NiveauEval(null, comp8, employee2,"Moyenne"));

		});
	}

}
